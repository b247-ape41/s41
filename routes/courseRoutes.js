const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseController");


/*
// Routes for create a course 
router.post("", (req, res) => {
     
        courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	
})
*/

// Route for create a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all the course
router.get("/all", (req, res) => {

	courseController.getAllCourse().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all the course
router.get("/active", (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Router for restrieving a specific course
router.get("/:courseId/details", (req, res) => {

	courseController.getCourse(req.params).then(resultFromController =>  res.send(resultFromController))
})

// Route for updating a course
router.put("/:id/update", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})


// Activity 
/*
router.put("/:id", auth.verify, (req, res) => {

	courseController.updateIsActive(req.params, req.body).then(resultFromController => res.send(resultFromController))
})*/


router.put("/:id/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController))
})
module.exports = router;