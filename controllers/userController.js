const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

const Course = require("../models/Course");


module.exports.checkEmailExists = (reqBody) => {

	 return User.find({email : reqBody.email}).then(result => {

	 	if(result.length > 0) {
	 		return true;
	 	} else {
	 		return false;
	 	};
	 });
};

module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)

	})

	return newUser.save().then((user, error) => {

		if(error) {

			return false;

		} else {

			return true;
		};
	});
};

// login user authenticatioon

module.exports.loginUser = (reqBody) => {

	// We use the "findOne" method instead of the  "find" method which return all records that match  the search criteria
	// The "findOne" method returns the first record in the collection that matches the search criteria
	return User.findOne({email : reqBody.email}).then(result => {

		//User doesn't exist
		if(result == null){
			return false;

		// User exist
		} else {

			// the "compareSync" method is used to compare a non encrypted password fromthe login form to the excrypted password retrived from the database and it returns "true" of "false" value depending on the result
			// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form pf is+Noun
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				// Generate an access token
				return {access : auth.createAccessToken(result)}
			} else {

				return false;
			}
		}
	})
}



// Activity 

module.exports.detailUser = (data) => {

	return User.findById(data.userId).then((result) => {
		
                result.password = "";
			    return result;

	})
}


// Async await will be used in ebrolling the user because we will need to update 2 seperate documents when enrolling a user
module.exports.enroll = async (data) => {

	// "async" is placed in the line 77 to declate that this is an async expression.

	// an async expressiion, is an expression that returns a promise.

	let isUserUpdated = await User.findById(data.userId).then(user => {user.enrollments.push({courseId : data.courseId});
		// "await" is written in line 83

		// Adds the coureId in the user's enrollements array
		user.enrollments.push({courseId : data.courseId});

		// save the update user information in the database
		return user.save().then((user, error) => {

			if(error){
				return false;
			} else {

				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// "await" is the written in line 98, so that tells our code to wait for the promisw to resolve, in this case for our course enrolles to be "push" in
		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		//save the updates course information in the database
		return course.save().then((course, err) => {

			if (err) {

				return false;
			} else {

				return true;
			};
		});
	});

      // to check if we are successful in fulling both promise, we will utilize the if statemen and the double ampersand as representative of ligal 
	if(isUserUpdated && isCourseUpdated){

		return true;
	} else {

		return false; 
	}
}