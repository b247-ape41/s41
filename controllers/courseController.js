const Course = require("../models/Course");


module.exports.addCourse = (data) => {

	if (data.isAdmin) {

		let newCourse = new Course({

			name : data.course.name,
			description : data.course.description,
			price : data.course.price

		});
       
     	return newCourse.save().then((course, error)=> {
           
         	if (error) {
         		
				return false;
	              
         	} else {

             	return	{message : "New course successfully created"}
        	}
   		})
	}

   let message = Promise.resolve({
   	        message: "User must be an admin to access this!"
   })

    return message.then((value) => {
    	return value;
    })
};

module.exports.getAllCourse = () => {

	return Course.find({}).then(result => {
		return result;
	});
};

module.exports.getAllActive = () => {

	return Course.find({isActive : true}).then(result => {
		return result;

	})
}

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

module.exports.updateCourse = (reqParams, reqBody) => {

	let updateCourse = {

		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price

	};

	return Course.findByIdAndUpdate(reqParams.id, updateCourse).then((course, err) => {

		if(err){
			return false;
		} else {
			let message = `successfully updated course Id - "${reqParams.id}"`

			return message;
		}
	})
}

/*// Activity 

module.exports.updateIsActive = (reqParams, reqBody) => {

	let updateIsActive = {

		isActive : reqBody.isActive
		
	};

	return Course.findByIdAndUpdate(reqParams.id, updateIsActive).then((course, err) => {

		if(err){

			return false;
		} else {

			return true;
		}
	})
}*/


// Activity 

module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.id, updateActiveField).then((course, err) => {

		if(err){

			return false;
		} else {

			return true;
		}
	})
}